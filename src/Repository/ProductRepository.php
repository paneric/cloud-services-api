<?php

namespace Paneric\CSA\Repository;

use Paneric\CSA\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private const OPERATORS = ['<', '=', '>'];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByAmount(int $amount, int $operatorKey = 1): array
    {
        $operator = self::OPERATORS[$operatorKey];

        return $this->createQueryBuilder('p')
            ->where('p.amount ' . $operator . ' :amount')
            ->setParameter('amount', $amount)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}

<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;
use Symfony\Component\HttpFoundation\Request;

class ProductAddAction extends Action
{
    public function add(Request $request): bool
    {
        $product = new Product();

        $attributes = $this->filterAttributes($request);

        if ($attributes === null) {
            return false;
        }

        $product->setName($attributes['name']);
        $product->setAmount($attributes['amount']);

        $this->manager->persist($product);
        $this->manager->flush();

        return true;
    }
}

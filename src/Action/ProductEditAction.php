<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;
use Symfony\Component\HttpFoundation\Request;

class ProductEditAction extends Action
{
    public function edit(Request $request, int $id): bool
    {
        $product = $this->manager
            ->getRepository(Product::class)
            ->find($id);

        if ($product === null) {
            return false;
        }

        $attributes = $this->getAttributes($request);

        if (isset($attributes['name']) && !empty($attributes['name'])) {
            $product->setName($attributes['name']);
        }

        if (
            isset($attributes['amount']) &&
            filter_var($attributes['amount'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE) !== null
        ) {
            $product->setAmount($attributes['amount']);
        }

        $this->manager->flush();

        return true;
    }
}

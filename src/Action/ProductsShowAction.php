<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;

class ProductsShowAction extends Action
{
    public function showAll(): array
    {
        return $this->manager
            ->getRepository(Product::class)
            ->findAll();
    }
}

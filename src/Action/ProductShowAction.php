<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;

class ProductShowAction extends Action
{
    public function show(int $id): ?object
    {
        return $this->manager
            ->getRepository(Product::class)
            ->find($id);
    }
}

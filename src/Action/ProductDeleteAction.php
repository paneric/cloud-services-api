<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;

class ProductDeleteAction extends Action
{
    public function delete(int $id): bool
    {
        $product = $this->manager
            ->getRepository(Product::class)
            ->find($id);

        if ($product === null) {
            return false;
        }

        $this->manager->remove($product);
        $this->manager->flush();

        return true;
    }
}

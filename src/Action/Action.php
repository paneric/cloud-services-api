<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class Action
{
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    protected function getAttributes(Request $request): ?array
    {
        if ($content = $request->getContent()) {
            return json_decode($content, true);
        }

        return null;
    }

    protected function filterAttributes(Request $request): ?array
    {
        $attributes = $this->getAttributes($request);

        if ($attributes === null) {
            return null;
        }

        if (!isset($attributes['name']) || empty($attributes['name'])) {
            return null;
        }

        if (
            !isset($attributes['amount']) ||
            filter_var($attributes['amount'], FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE) === null
        ) {
            return null;
        }

        return $attributes;
    }
}

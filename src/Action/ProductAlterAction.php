<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;
use Symfony\Component\HttpFoundation\Request;

class ProductAlterAction extends Action
{
    public function alter(Request $request, int $id): bool
    {
        $product = $this->manager
            ->getRepository(Product::class)
            ->find($id);

        if ($product === null) {
            return false;
        }

        $attributes = $this->filterAttributes($request);

        if ($attributes === null) {
            return false;
        }

        $product->setName($attributes['name']);
        $product->setAmount($attributes['amount']);

        $this->manager->flush();

        return true;
    }
}

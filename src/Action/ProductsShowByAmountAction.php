<?php

declare(strict_types=1);

namespace Paneric\CSA\Action;

use Paneric\CSA\Entity\Product;

class ProductsShowByAmountAction extends Action
{
    public function showByAmount(int $amount, int $operatorKey = 1): ?array
    {
        return $this->manager
            ->getRepository(Product::class)
            ->findByAmount($amount, $operatorKey);
    }
}

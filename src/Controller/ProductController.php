<?php

declare(strict_types=1);

namespace Paneric\CSA\Controller;

use Paneric\CSA\Action\ProductAddAction;
use Paneric\CSA\Action\ProductAlterAction;
use Paneric\CSA\Action\ProductDeleteAction;
use Paneric\CSA\Action\ProductEditAction;
use Paneric\CSA\Action\ProductShowAction;
use Paneric\CSA\Action\ProductsShowAction;
use Paneric\CSA\Action\ProductsShowByAmountAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController
{
    public function show(ProductShowAction $action, int $id): JsonResponse
    {
        $response = new JsonResponse();

        $product = $action->show($id);

        if ($product !== null) {
            return $response->setData($product);
        }

        $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);

        return $response->setData([
            'error' => 'Invalid query parameter',
            'code' => 400
        ]);
    }

    public function showAll(ProductsShowAction $action): JsonResponse
    {
        $response = new JsonResponse();

        return $response->setData(
            $action->showAll()
        );
    }

    public function showByAmount(ProductsShowByAmountAction $action, int $amount, int $operator = 1): JsonResponse
    {
        $response = new JsonResponse();

        return $response->setData(
            $action->showByAmount($amount, $operator)
        );
    }

    public function add(Request $request, ProductAddAction $action): JsonResponse
    {
        $response = new JsonResponse();

        if ($action->add($request)) {
            return $response->setData([
                'response_code' => 0
            ]);
        }

        $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);

        return $response->setData([
            'error' => 'Missing input data',
            'code' => 400
        ]);
    }

    public function alter(Request $request, ProductAlterAction $action, int $id): JsonResponse
    {
        $response = new JsonResponse();

        if ($action->alter($request, $id)) {
            return $response->setData([
                'response_code' => 0
            ]);
        }

        $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);

        return $response->setData([
            'error' => 'Missing input data or invalid query parameter',
            'code' => 400
        ]);
    }

    public function edit(Request $request, ProductEditAction $action, int $id): JsonResponse
    {
        $response = new JsonResponse();

        if ($action->edit($request, $id)) {
            return $response->setData([
                'response_code' => 0
            ]);
        }

        $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);

        return $response->setData([
            'error' => 'Invalid query parameter',
            'code' => 400
        ]);
    }

    public function delete(ProductDeleteAction $action, int $id): JsonResponse
    {
        $response = new JsonResponse();

        if ($action->delete($id)) {
            return $response->setData([
                'response_code' => 0
            ]);
        }

        $response->setStatusCode(JsonResponse::HTTP_BAD_REQUEST);

        return $response->setData([
            'error' => 'Invalid query parameter',
            'code' => 400
        ]);
    }
}

# Cloud Services API

Junior PHP Developer test challenge

## Required

* PHP >= 7.2.5

## Tested environment

* **Linux Ubuntu 18.04.5**
* **Symfony 4.20.1** with installed:
    * **symfony/orm-pack**
    * **symfony/maker-bundle**
* **MySQL 5.7.32**

## Folder structure

* **cloud-services-api**
    * **config**
        * routes.yaml
    * **src**
        * **Action**
        * **Controller**
        * **Entity**
        * **Repository**
    * composer.json

## Practices and principles:

#### "Thin controller, fat service"

There has been a "thin controller fat service" practice implemented, slightly modified with single "service action"
approach but not in relation to controllers but services methods.  
Such approach allows keeping controllers body the thinnest possible and hand all logic over its methods related "service
actions". Name of the "service action" is due to no better idea at the moment.

#### "Separation of concerns"
By personal preference and according to above principle, routes annotations within a controller are avoided and 
configured in appropriate configuration file even despite commonly applied practice among Symfony developers.  

In case of Doctrine the situation looks a little more complicated, so let it be, especially that in its case annotations
makes things much, much more smooth and handy.

## Installation

From within existing Symfony project folder:

```sh
$ composer require paneric/cloud-services-api
```

## Symfony project configuration

(1) Adapt routes.yaml with import directive of package routing resources:

_config/routes.yaml_
```yaml
cloud_services_api:
  resource: ../vendor/paneric/cloud-services-api/config/routes.yaml
```

(2) Adapt services.yaml with entries:

_config/services.yaml_
```yaml
parameters:
    ...
services:
    ...
    Paneric\CSA\:
        resource: '../vendor/paneric/cloud-services-api/src/'
        exclude:
            - '../vendor/paneric/cloud-services-api/src/Entity/'

    Paneric\CSA\Controller\:
        resource: '../vendor/paneric/cloud-services-api/src/Controller/'
        tags: ['controller.service_arguments']
    ...
```

(3) Adapt application doctrine.yaml with entry:

_config/packages/doctrine.yaml_
```yaml
doctrine:
    ...
    orm:
        ...
        mappings:
            ...
            Paneric\CSA:
                is_bundle: false
                type: annotation
                dir: '%kernel.project_dir%/vendor/paneric/cloud-services-api/src/Entity'
                prefix: 'Paneric\CSA\Entity'
                alias: Paneric\CSA
```

(4) Make and run migrations:

```sh
$ bin/console make:migration

$ bin/console doctrine:migrations:migrate
```

## Endpoints

| n°      | Path                                            | Method |
| ------- |:------------------------------------------------|--------|
| (1)     | /api/products                                   | GET    |
| (2) (*) | /api/products/show-by-amount/{amount}/{operator}| GET    |
| (3)     | /api/product/{id}                               | GET    |
| (4)     | /api/product                                    | POST   |
| (5)     | /api/product/{id}                               | DELETE |
| (6)     | /api/product/{id}                               | PUT    |
| (7)     | /api/product/{id}                               | PATCH  |

(2)(*) Remarks:  

* amount: amount of products (integer)
* operator: sql query operator index as following:
    * 0 related to "<"
    * 1 related to "="
    * 2 related to ">"
    * **default value** = 1
    
## Command line CURL requests examples

#### (1) Fetching all products from data base:

```sh
$ curl -v http://127.0.0.1:8080/api/products
```

#### (2) Fetching products by "amount" field from data base:
  
(2.1) Fetching products by the **amount = 0**
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/0
```
or:
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/0/1
```
(2.2) Fetching products by the **amount > 0**
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/0/2
```
(2.3) Fetching products by the **amount < 4**
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/4/0
```
 
#### (3) Fetching product by _id = 1_ from data base:
```sh
$ curl -v http://127.0.0.1:8080/api/product/1
```

#### (4) Adding new product:
```sh
$ curl -d '{"name":"Produkt 1","amount":4}' -H 'Content-Type: application/json' -X POST http://127.0.0.1:8080/api/product
```

#### (5) Deleting product by _id = 1_ from data base:
```sh
$ curl -X DELETE http://127.0.0.1:8080/api/product/1
```

#### (6) Altering product by _id = 1_ in data base: (all fields required)
```sh
$ curl -d '{"name":"Produkt 1","amount":12}' -H 'Content-Type: application/json' -X PUT http://127.0.0.1:8080/api/product/1
```

#### (7) Updating product by _id = 1_ in data base:
```sh
$ curl -d '{"amount":8}' -H 'Content-Type: application/json' -X PATCH http://127.0.0.1:8080/api/product/1
```

## Final thoughts

To fetch all the products available:
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/0/2
```

To fetch exhausted products:
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/0
```

To fetch products with its amount bigger than 5:
```sh
$ curl -v http://127.0.0.1:8080/api/products/show-by-amount/5/2
```